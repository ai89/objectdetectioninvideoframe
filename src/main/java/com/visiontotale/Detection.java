package com.visiontotale;

import lombok.extern.slf4j.Slf4j;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.OpenCVFrameConverter;
//import org.bytedeco.opencv.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Mat;

import java.io.File;
import java.io.IOException;

import static org.bytedeco.javacpp.opencv_highgui.imshow;
import static org.bytedeco.javacpp.opencv_highgui.waitKey;
import static org.bytedeco.javacpp.opencv_highgui.destroyAllWindows;

@Slf4j
public class Detection {
  private volatile Mat[] v = new Mat[1];
  private static String VIDEO_FILE = "src/data/videoSample.mp4";
  private static String WINDOWS_FRAME = "Object Detection from Video";

  public void execute(String[] args) throws Exception {
    TinyYoloModel model = new TinyYoloModel();
    startRealTimeVideoDetection(model);
  }

  /**
   * Method that performs the real time object detection in a video frame.
   *
   * @param tinyYoloModel the tiny YOLO model used to perform the detection.
   * @throws Exception the exception thrown in case there is any issue.
   */
  private void startRealTimeVideoDetection(TinyYoloModel tinyYoloModel) throws Exception {
    if (new File(VIDEO_FILE).exists()) {
      FFmpegFrameGrabber frameGrabber = new FFmpegFrameGrabber(VIDEO_FILE);
      //frameGrabber.setFormat("mp4");
      frameGrabber.start();

      Frame frame;
      double frameRate = frameGrabber.getFrameRate();
      log.info(
          String.format("The inputted video clip has %s frames", frameGrabber.getLengthInFrames()));
      log.info(String.format("The inputted video clip has frame rate of %s", frameRate));

      try {
        for (int i = 1; i < frameGrabber.getLengthInFrames(); i += (int) frameRate) {
          frameGrabber.setFrameNumber(i);
          frame = frameGrabber.grab();
          v[0] = new OpenCVFrameConverter.ToMat().convert(frame);
          tinyYoloModel.markObjectWithBoundingBox(
              v[0], frame.imageWidth, frame.imageHeight, true, WINDOWS_FRAME);
          imshow(WINDOWS_FRAME, v[0]);

          char key = (char) waitKey(20);
          // Exit on escape:
          if (key == 27) {
            destroyAllWindows();
            break;
          }
        }
      } catch (IOException e) {
        e.printStackTrace();
      } finally {
        frameGrabber.stop();
      }
      frameGrabber.close();

    } else {
      log.info("There is no video");
    }
  }

  /**
   * @param args
   * @throws Exception
   */
  public static void main(String[] args) throws Exception {
    new Detection().execute(args);
  }
}
