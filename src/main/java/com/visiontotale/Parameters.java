package com.visiontotale;

import lombok.Getter;

import java.util.Map;

/** Parameters */
@Getter
public class Parameters {

  /** Detection threshold */
  private double detectionThreshold = 0.5;

  /** Width of the video frame */
  private int vfWidth = 640;

  /** Height of the video frame */
  private int vfHeight = 360;

  /** Grid width */
  private int gWidth = 13;

  /** Grid Height */
  private int gHeight = 13;

  /** Number of color channels */
  private int nChannels = 3;

  /** Object labels */
  private Map<Integer, String> labels =
      Map.ofEntries(
          Map.entry(1, "aeroplane"),
          Map.entry(2, "bicycle"),
          Map.entry(3, "bird"),
          Map.entry(4, "boat"),
          Map.entry(5, "bottle"),
          Map.entry(6, "bus"),
          Map.entry(7, "car"),
          Map.entry(8, "cat"),
          Map.entry(9, "chair"),
          Map.entry(10, "cow"),
          Map.entry(11, "dining table"),
          Map.entry(12, "dog"),
          Map.entry(13, "horse"),
          Map.entry(14, "motorbike"),
          Map.entry(15, "person"),
          Map.entry(16, "potted plant"),
          Map.entry(17, "sheep"),
          Map.entry(18, "sofa"),
          Map.entry(19, "train"),
          Map.entry(20, "tvmonitor"));
}
