package com.visiontotale;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Point;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.datavec.image.loader.NativeImageLoader;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.layers.objdetect.DetectedObject;
import org.deeplearning4j.nn.layers.objdetect.Yolo2OutputLayer;
import org.deeplearning4j.util.ModelSerializer;
import org.deeplearning4j.zoo.model.TinyYOLO;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.bytedeco.javacpp.opencv_highgui.imshow;
import static org.bytedeco.javacpp.opencv_imgproc.putText;
import static org.bytedeco.javacpp.opencv_imgproc.rectangle;
import static org.bytedeco.javacpp.opencv_core.FONT_HERSHEY_DUPLEX;

@Slf4j
@Getter
public class TinyYoloModel {
  private ComputationGraph model;
  private List<DetectedObject> predictedObjects;
  private static String MODEL_FILE_NAME = "src/data/Object_Detection_Model.zip";
  private Parameters params = new Parameters();

  public TinyYoloModel() throws IOException {

    if (new File(MODEL_FILE_NAME).exists()) {
      log.info("Loading existing model...");
      model = ModelSerializer.restoreComputationGraph(MODEL_FILE_NAME);
    } else {
      log.info("Downloading the pretrained model...");
      model = (ComputationGraph) TinyYOLO.builder().build().initPretrained();
      ModelSerializer.writeModel(model, MODEL_FILE_NAME, true);
    }
  }

  /**
   * Marking objects with bounding box
   *
   * @param file the video file
   * @param imageWidth the width of the video frame
   * @param imageHeight the height of the video frame
   * @param newBoundingBOx is it a new bounding box ?
   * @param winName the name of the video frame
   * @throws Exception the exception thrown in case there is any issue.
   */
  public void markObjectWithBoundingBox(
      Mat file, int imageWidth, int imageHeight, boolean newBoundingBOx, String winName)
      throws Exception {

    Yolo2OutputLayer outputLayer = (Yolo2OutputLayer) model.getOutputLayer(0);

    if (newBoundingBOx) {
      INDArray indArray = prepareImage(file, params.getVfWidth(), params.getVfHeight());
      INDArray results = model.outputSingle(indArray);
      predictedObjects = outputLayer.getPredictedObjects(results, params.getDetectionThreshold());
      log.info(String.format("Result =  %s", predictedObjects));
      markObjectWithBoundingBox(
          file, params.getGWidth(), params.getGHeight(), imageWidth, imageHeight);
    } else {
      markObjectWithBoundingBox(
          file, params.getGWidth(), params.getGHeight(), imageWidth, imageHeight);
    }
    imshow(winName, file);
  }

  /**
   * Method used to pre-process each image of the video frame
   *
   * @param file the video file
   * @param vfWidth the width of the video frame
   * @param vfHeight the height of the video frame
   * @throws IOException the exception thrown in case there is any issue.
   */
  private INDArray prepareImage(Mat file, int vfWidth, int vfHeight) throws IOException {
    NativeImageLoader loader = new NativeImageLoader(vfHeight, vfWidth, params.getNChannels());
    ImagePreProcessingScaler imagePreProcessingScaler = new ImagePreProcessingScaler(0, 1);
    INDArray indArray = loader.asMatrix(file);
    imagePreProcessingScaler.transform(indArray);
    return indArray;
  }

  /**
   * Marking objects with bounding box
   *
   * @param file the video file
   * @param gWidth the grid width of the image
   * @param gHeight the grid height of the image
   * @param imageWidth the width of the video frame
   * @param imageHeight the height of the video frame
   * @throws Exception the exception thrown in case there is any issue.
   */
  private void markObjectWithBoundingBox(
      Mat file, int gWidth, int gHeight, int imageWidth, int imageHeight) {
    if (predictedObjects != null) {
      ArrayList<DetectedObject> detectedObjects = new ArrayList<>(predictedObjects);
      while (!detectedObjects.isEmpty()) {
        Optional<DetectedObject> max =
            detectedObjects.stream()
                .max((o1, o2) -> ((Double) o1.getConfidence()).compareTo(o2.getConfidence()));
        if (max.isPresent()) {
          DetectedObject maxObjectDetect = max.get();
          removeObjectsIntersectingWithMax(detectedObjects, maxObjectDetect);
          detectedObjects.remove(maxObjectDetect);
          markObjectWithBoundingBox(
              file, gWidth, gHeight, imageWidth, imageHeight, maxObjectDetect);
        }
      }
    }
  }

  private void markObjectWithBoundingBox(
      Mat file,
      int gWidth,
      int gHeight,
      int imageWidth,
      int imageHeight,
      DetectedObject objectDetect) {

    double[] xy1 = objectDetect.getTopLeftXY();
    double[] xy2 = objectDetect.getBottomRightXY();

    int predictedClass = objectDetect.getPredictedClass();

    int x1 = (int) Math.round(imageWidth * xy1[0] / gWidth);
    int y1 = (int) Math.round(imageHeight * xy1[1] / gHeight);
    int x2 = (int) Math.round(imageWidth * xy2[0] / gWidth);
    int y2 = (int) Math.round(imageHeight * xy2[1] / gHeight);
    rectangle(file, new Point(x1, y1), new Point(x2, y2), Scalar.RED);
    putText(
        file,
        params.getLabels().get(predictedClass),
        new Point(x1 + 2, y2 - 2),
        FONT_HERSHEY_DUPLEX,
        1,
        Scalar.GREEN);
  }

  /**
   * Marking objects with bounding box
   *
   * @param detectedObjects the video file
   * @param maxObjectDetect the grid width of the image
   */
  private void removeObjectsIntersectingWithMax(
      ArrayList<DetectedObject> detectedObjects, DetectedObject maxObjectDetect) {
    double[] bottomRightXY1 = maxObjectDetect.getBottomRightXY();
    double[] topLeftXY1 = maxObjectDetect.getTopLeftXY();
    List<DetectedObject> removeIntersectingObjects = new ArrayList<>();

    for (DetectedObject detectedObject : detectedObjects) {
      double[] topLeftXY = detectedObject.getTopLeftXY();
      double[] bottomRightXY = detectedObject.getBottomRightXY();
      double iox1 = Math.max(topLeftXY[0], topLeftXY1[0]);
      double ioy1 = Math.max(topLeftXY[1], topLeftXY1[1]);

      double iox2 = Math.min(bottomRightXY[0], bottomRightXY1[0]);
      double ioy2 = Math.min(bottomRightXY[1], bottomRightXY1[1]);

      double inter_area = (ioy2 - ioy1) * (iox2 - iox1);

      double box1_area = (bottomRightXY1[1] - topLeftXY1[1]) * (bottomRightXY1[0] - topLeftXY1[0]);
      double box2_area = (bottomRightXY[1] - topLeftXY[1]) * (bottomRightXY[0] - topLeftXY[0]);

      double union_area = box1_area + box2_area - inter_area;
      double iou = inter_area / union_area;

      if (iou > 0.5) {
        removeIntersectingObjects.add(detectedObject);
      }
    }
    detectedObjects.removeAll(removeIntersectingObjects);
  }
}
